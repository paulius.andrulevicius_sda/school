import java.time.LocalDate;
import java.time.LocalTime;

public class Requires {
    public static class Str{
        public static void NotNullOrEmpty(String value, String name){
            if(value == null || value.isEmpty()){
                throw new IllegalArgumentException(name + " cannot be null or empty");
            }
        }
    }
    public static class DateTime{
        public static void NotFuture(LocalDate dateTime, String argumentName){
            boolean isFutureDateTime = dateTime.isAfter(LocalDate.now());
            if(dateTime.isAfter(LocalDate.now())){
                throw new FutureBirthdayExeption(dateTime, argumentName);
            }
        }
    }
}
