import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Demo {
    public static void main(String[] args) {
        Person person = new Person("Vytas", "Kazkas", LocalDate.now().minusYears(20), 65, 174, Gender.MALE );
        Person person1 = new Person("Rasa", "Pavarde", LocalDate.now().minusYears(23), 50, 154, Gender.FEMALE );
        Person person2 = new Person("Andrius", "Andraitis", LocalDate.now().minusMonths(20), 80, 198, Gender.MALE );

//        Person[] people = new Person[3];
//        peaple[0] = person;
//        peaple[1] = person1;
//        peaple[2] = person2;

        Person[] people = {person, person1, person2};

        Stream<Person> peopleStream = Arrays.stream(people);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(p -> System.out.println(p));
        //Person[] adultsArray = adultsStream.toArray(Person[] :: new);

        System.out.println("hello reviewer");

    }
}
