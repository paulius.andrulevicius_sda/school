import java.time.LocalDate;

public class FutureBirthdayExeption extends RuntimeException{
    public FutureBirthdayExeption(LocalDate futureDate, String argumentName){
        super(argumentName + "DateTime cannot be future. Was " + futureDate);
    }
}
